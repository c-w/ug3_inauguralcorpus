#################### FNLP Assignment 1 ######$$$$##########
from nltk import FreqDist
from nltk.corpus import inaugural
from nltk.corpus import stopwords
import pylab


#### Constants ####
STOPS = list(stopwords.words('english'))


#### Function for Question 4 ####
def number_of_word_types(fileid):
    words = inaugural.words(fileid)
    unique_words = _vocabulary(words)
    num_word_types = len(unique_words)
    return num_word_types


def rank(fd, sample):
    """
    returns the rank of the word |sample| in the frequency distribution |fd|
    """
    # |c| is how often |sample| occurs in |fd|
    c = fd[sample]
    # let m be the most common word in |fd| - we can retrieve m with fd.max()
    # let Cm be the number of occurences of m in |fd| - Cm is given by fd[m]
    # remembering that the function range(a, b) returns a list of numbers
    # [a, a+1, a+2, ..., b-2, b-1], we thus know that the call to range(...)
    # below constructs the list of numbers [c+1, c+2, c+3, ..., Cm-1, Cm]
    # fd.Nr(r) returns how many words in |fd| occur r times
    # the code within the call to sum(...) below thus constructs a list of
    # numbers [c1, c2, ..., cx], where c1 is the number of words that occur c+1
    # times in |fd|, c2 is the number of words that occur c+2 times in |fd|, and
    # so forth (cx is the number of words that occur Cm times in |fd| - N.B.
    # this must not necessarily be equal to Cm because more than one word can be
    # tied for most common).
    # |p| is the sum c1 + c2 + ... + cx which means that |p| is number of words
    # (not word types!) that occur more frequently than |sample| in |fd|
    # N.B. if |sample| is the most common word, then |p| is 0
    p = sum(fd.Nr(r) for r in range(c + 1, fd[fd.max()] + 1))
    # we thus define the rank of |sample| as being the number of words that
    # occur more frequently than |sample| plus half the number of words that
    # occur as frequently as |sample|
    return p + ((fd.Nr(c) + 1) / 2.0)


#### Trivial Convenience Functions (used all over the place) ####
def _nopunct(li): return [elem.lower() for elem in li if elem.isalpha()]
def _vocabulary(li): return set(_nopunct(li))
def _lengths(li): return [len(elem) for elem in li]
def _avg(li): return float(sum(li)) / len(li)
def _firsts(li): return [fst for (fst, _) in li]
def _nostops(li): return [w.lower() for w in li if w.lower() not in STOPS]
def _str(li): return ', '.join(li)


def answers():
    _rvals = []

    #### Question 1 ####
    print '##### Question 1 #####'
    print '(see code - lines 64-65)'
    print '(NB: the two variables are returned by this function)'
    _bush01 = inaugural.words('2001-Bush.txt')
    bush01_word_lengths = _lengths(_vocabulary(_bush01))
    fd_bush01_words = FreqDist(_nopunct(_bush01))
    _rvals.append(bush01_word_lengths)
    _rvals.append(fd_bush01_words)

    #### Question 2 ####
    print '\n##### Question 2 #####'
    bush01_top10_words = _firsts(fd_bush01_words.items()[:10])
    bush01_average_word_lengths = _avg(bush01_word_lengths)
    _obama09 = inaugural.words('2009-Obama.txt')
    _fd_obama09_words = FreqDist(_nopunct(_obama09))
    _obama09_word_lengths = _lengths(_vocabulary(_obama09))
    obama09_top10_words = _firsts(_fd_obama09_words.items()[:10])
    obama09_average_word_lengths = _avg(_obama09_word_lengths)
    print 'top10 words Bush (2001): ', _str(bush01_top10_words)
    print 'top10 words Obama (2009):', _str(obama09_top10_words)
    print 'average word length Bush (2001): ', bush01_average_word_lengths
    print 'average word length Obama (2009):', obama09_average_word_lengths

    #### Question 3 ####
    print '\n##### Question 3 #####'
    bush01_token_lengths = _avg(_lengths(_nopunct(_bush01)))
    obama09_token_lengths = _avg(_lengths(_nopunct(_obama09)))
    print 'average token length Bush (2001): ', bush01_token_lengths
    print 'average token length Obama (2009):', obama09_token_lengths

    #### Question 4 ####
    print '\n##### Question 4 #####'
    for _fileid in inaugural.fileids():
        _year = int(_fileid.split('-')[0])
        _vocab_size = number_of_word_types(_fileid)
        print 'year %d: %d word types' % (_year, _vocab_size)

    #### Question 5 ####
    print '\n##### Question 5 #####'
    fd_bush01_nostop = FreqDist(_nostops(_nopunct(_bush01)))
    fd_obama09_nostop = FreqDist(_nostops(_nopunct(_obama09)))
    bush01_top10_nostop = _firsts(fd_bush01_nostop.items()[:10])
    obama09_top10_nostop = _firsts(fd_obama09_nostop.items()[:10])
    print 'top10 non-stop-words Bush (2001): ', _str(bush01_top10_nostop)
    print 'top10 non-stop-words Obama (2009):', _str(obama09_top10_nostop)

    #### Question 6 ####
    print '\n##### Question 6 #####'
    _wash89 = inaugural.words('1789-Washington.txt')
    fd_wash89_nostop = FreqDist(_nostops(_nopunct(_wash89)))
    wash89_top10_nostop = _firsts(fd_wash89_nostop.items()[:10])
    print 'top10 non-stop-words Washington (1789):', _str(wash89_top10_nostop)

    #### Question 7 ####
    print '\n##### Question 7 #####'
    wash89_rank_country = rank(fd_wash89_nostop, 'country')
    obama09_rank_country = rank(fd_obama09_nostop, 'country')
    bush01_rank_country = rank(fd_bush01_nostop, 'country')
    print 'rank of "country" in Washington (1789):', wash89_rank_country
    print 'rank of "country" in Obama (2009):', obama09_rank_country
    print 'rank of "country" in Bush (2001):', bush01_rank_country

    #### Question 8 ####
    print '\n##### Question 7 #####'
    print '(see comments in "rank" function on lines 20-45)'
    
    #### Question 9 ####
    print '\n##### Question 9 #####'
    print '(see plot)'
    ff = inaugural.fileids()
    fdd = {}
    _years = []
    for _fileid in ff:
        fdd[_fileid] = FreqDist(_nostops(inaugural.words(_fileid)))
        _years.append(_fileid[0:4])
    pylab.plot([(lambda d: len(d) / float(d.N()))(fdd[f]) for f in ff])
    pylab.xticks(range(len(ff)), _years, rotation=90)
    pylab.xlim(0, len(ff) - 1)
    pylab.ylabel('ratio of word types to tokens (without stop-words)')
    pylab.xlabel('time')
    pylab.title('f(time) = #(word types) / #(word tokens)')
    pylab.show()

    #### Question 10 ####
    print '\n##### Question 10 #####'
    print '(see plot)'
    obama09top10_butnot_wash89top10 = [word for word in obama09_top10_nostop
        if word in fd_wash89_nostop and word not in wash89_top10_nostop]
    wash89top10_butnot_obama09top10 = [word for word in wash89_top10_nostop
        if word in fd_obama09_nostop and word not in obama09_top10_nostop]
    obama09_word = 'world'
    wash89_word = 'government'
    assert(wash89_word in wash89top10_butnot_obama09top10)
    assert(obama09_word in obama09top10_butnot_wash89top10)
    normalisation_justification = (\
    "We normalise for different sizes in vocabulary by dividing the rank of "
    "some word by the size of the vocabulary in that speech"
    "Since rank is in relation with vocabulary size, this is similar to "
    "getting the maximum rank over all speeches and dividing each rank by that "
    "quantity")
    print normalisation_justification
    _normalised_rank = lambda f, w: min(1, rank(fdd[f], w) / \
        float(len(_vocabulary(_nostops(fdd[f])))))
    pylab.plot([_normalised_rank(f, obama09_word) for f in ff],
        label=obama09_word, color='b')
    pylab.plot([_normalised_rank(f, wash89_word) for f in ff],
        label=wash89_word, color='r')
    pylab.xticks(range(len(ff)), _years, rotation=90)
    pylab.xlim(0, len(ff) - 1)
    pylab.ylabel('normalised word rank (lower is better)')
    pylab.xlabel('time')
    pylab.title('f(time) = word rank / vocabulary size')
    pylab.legend()
    pylab.show()

    #### Question 11 ####
    print '\n##### Question 11 #####'
    observations_on_plots = (\
    "We observe that the rank of 'world' is noisy when observed on the level "
    "of some individual year/inaugural speech. However, when looking at the "
    "larger picture, a trend emerges: 'world''s rank is consistenlty getting "
    "higher over time - an indicator for an ever-globalising and shrinking "
    "world?"
    "\n"
    "We observe that 'government' is a consistently highly ranked word across "
    "time - expcept for some few inaugural speeches where it has a very low "
    "rank. Those speeches are around the early 1800s (abolishment of slavery),"
    " 1860s-70s (US civil war), the early 1900s (Word War One), and 1937-1981"
    "(World War Two + Cold War) - it would seem that presidents don't want to "
    "remind their subjugates of the government during hard times. Outliers to "
    "this theory can be explained easily (e.g. somewhat high rank of "
    "'government' in 1949 = a certain 'evil government' being defeated).")
    print observations_on_plots

    return _rvals
 

if __name__ == '__main__':
    _ = answers()
